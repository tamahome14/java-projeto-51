/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utfpr.ct.dainf.if62c.pratica;

/**
 *
 * @author jorgejunior
 */
public class Matriz {
    // a matriz representada por esta classe
    private final double[][] mat;
    
    /**
     * Construtor que aloca a matriz.
     * @param m O número de linhas da matriz.
     * @param n O número de colunas da matriz.
     * @throws utfpr.ct.dainf.if62c.pratica.MatrizInvalidaException
     */
    public Matriz(int m, int n) throws MatrizInvalidaException  {
        if (m == 0 || n == 0) {
            throw new MatrizInvalidaException(m, n);
        } else {
            mat = new double[m][n];            
        }        
    }
    
    /**
     * Retorna a matriz representada por esta classe.
     * @return A matriz representada por esta classe
     */
    public double[][] getMatriz() {
        return mat;
    }
    
    /**
     * Retorna a matriz transposta.
     * @return A matriz transposta.
     * @throws utfpr.ct.dainf.if62c.pratica.MatrizInvalidaException
     */
    public Matriz getTransposta() throws MatrizInvalidaException {
        Matriz t = new Matriz(mat[0].length, mat.length);
        for (int i = 0; i < mat.length; i++) {
            for (int j = 0; j < mat[i].length; j++) {
                t.mat[j][i] = mat[i][j];
            }
        }
        return t;
    }
    
    /**
     * Retorna a soma desta matriz com a matriz recebida como argumento.
     * @param m A matriz a ser somada
     * @return A soma das matrizes
     * @throws utfpr.ct.dainf.if62c.pratica.MatrizInvalidaException
     * @throws utfpr.ct.dainf.if62c.pratica.MatrizesIncompativeisException
     */
    public Matriz soma(Matriz m) throws MatrizInvalidaException {
        
        Matriz s = new Matriz(mat.length, mat[0].length);
        double[][] soma = s.getMatriz();
        double[][] aux = m.getMatriz();
        
         for (int i = 0; i < mat.length; i++) {
            for (int j = 0; j < mat[i].length; j++) {
                soma[i][j] = mat[i][j] + aux [i][j];
            }
        }
        return s;
    }

    /**
     * Retorna o produto desta matriz com a matriz recebida como argumento.
     * @param m A matriz a ser multiplicada
     * @return O produto das matrizes
     * @throws utfpr.ct.dainf.if62c.pratica.MatrizInvalidaException
     * @throws utfpr.ct.dainf.if62c.pratica.ProdMatrizesIncompativeisException
     */
    public Matriz prod(Matriz m) throws MatrizInvalidaException, ProdMatrizesIncompativeisException {
        
        Matriz t = new Matriz(mat.length, mat[0].length); 
        double[][] aux = t.getTransposta().getMatriz();
        
        Matriz p = new Matriz(mat.length, aux[0].length);
        double[][] prod = p.getMatriz();

        
        
            
        for (int i = 0; i < mat.length; i++) {
           for (int j = 0; j < mat.length; j++) {
               for (int k = 0; k < mat[i].length; k++) {
                   prod[i][j] += mat[i][k] * aux [k][j];
               }
           }
        }

        return p;
        
        
       
        
        
    }

    /**
     * Retorna uma representação textual da matriz.
     * Este método não deve ser usado com matrizes muito grandes
     * pois não gerencia adequadamente o tamanho do string e
     * poderia provocar um uso excessivo de recursos.
     * @return Uma representação textual da matriz.
     */
    @Override
    public String toString() {
        StringBuilder s = new StringBuilder();
        for (double[] linha: mat) {
            s.append("[ ");
            for (double x: linha) {
                s.append(x).append(" ");
            }
            s.append("]");
        }
        return s.toString();
    }
    
}
