/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utfpr.ct.dainf.if62c.pratica;

/**
 *
 * @author jorgejunior
 */
public class MatrizInvalidaException extends Exception {
    private int linhas;
    private int colunas;
    
    public MatrizInvalidaException(int linhas, int colunas) {
        super(String.format("Matriz de %dx%d não pode ser criada", linhas, colunas));
    }

    /**
     * @return the linhas
     */
    public int getLinhas() {
        return linhas;
    }

    /**
     * @param linhas the linhas to set
     */
    public void setLinhas(int linhas) {
        this.linhas = linhas;
    }

    /**
     * @return the colunas
     */
    public int getColunas() {
        return colunas;
    }

    /**
     * @param colunas the colunas to set
     */
    public void setColunas(int colunas) {
        this.colunas = colunas;
    }

    public int getNumLinhas() {
        return this.getLinhas();
    }
    
    public int getNumColunas() {
        return this.getColunas();
    }
}
