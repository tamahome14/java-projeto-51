
import utfpr.ct.dainf.if62c.pratica.Matriz;
import utfpr.ct.dainf.if62c.pratica.MatrizInvalidaException;
import utfpr.ct.dainf.if62c.pratica.MatrizesIncompativeisException;
import utfpr.ct.dainf.if62c.pratica.ProdMatrizesIncompativeisException;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author jorgejunior
 */
public class Pratica51 {
    public static void main(String[] args) throws MatrizInvalidaException, MatrizesIncompativeisException {
        try {
            Matriz orig = new Matriz(3, 2);
            double[][] m = orig.getMatriz();
            m[0][0] = 0.0;
            m[0][1] = 0.1;
            m[1][0] = 1.0;
            m[1][1] = 1.1;
            m[2][0] = 2.0;
            m[2][1] = 2.1;

            Matriz nova = new Matriz(4, 1);
            double[][] n = nova.getMatriz();
            n[0][0] = 0.0;
            n[1][0] = 1.0;
            n[2][0] = 2.0;
            n[3][0] = 3.0;

            Matriz somada = orig.soma(nova);
            Matriz multiplicada = orig.prod(nova);
            System.out.println("Matriz original: " + orig);
            System.out.println("Matriz somada: " + somada);
            System.out.println("Matriz multiplicada: " + multiplicada);
        } catch(MatrizInvalidaException miex){
            System.out.println(miex.getMessage());
        } catch(ProdMatrizesIncompativeisException prodex) {
            System.out.println(prodex.getMessage());
        }
    }    
}
